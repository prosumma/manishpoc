//
//  TipCalculatorTests.swift
//  TipCalculatorTests
//
//  Created by Manish Kumar on 11/02/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import XCTest
@testable import TipCalculator
class TipCalculatorTests: XCTestCase {
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
        
        func testMyButtonAction(){
        
            ViewController *mainView =  [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
            [mainView view];
            
            id mock =  [OCMockObject partialMockForObject:mainView];
            
            //testButtonPressed IBAction should be triggered
            [[mock expect] testButtonPressed:[OCMArg any]];
            
            //simulate button press
            [mainView.testButton sendActionsForControlEvents: UIControlEventTouchUpInside];
            
            [mock verify];
    }
    
}
